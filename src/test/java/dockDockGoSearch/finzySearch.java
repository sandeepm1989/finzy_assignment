package dockDockGoSearch;

import org.testng.annotations.*;

import utilities.initializeDriver;
import java.io.IOException;
import pom.*;
import dataProvider.ConfigFileReader;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class finzySearch {
	
	WebDriver driver;
	homePage hp;
	searchResPage srp;
	initializeDriver id;
	ConfigFileReader cfr;
	
  @Test
  public void duckDuckGoSearch() throws InterruptedException {
	  
	  // Validating Page Title
	  String strPageTitle = driver.getTitle();
	  Assert.assertEquals(strPageTitle, "DuckDuckGo — Privacy, simplified.");
	  
	  // Taking input for search
	  String strSearchInput = cfr.getSearchInput();													
	  
	  //Calling function to perform search
	  hp = new homePage(driver);
	  hp.performDuckDuckGoSearch(strSearchInput);
	  
	  //Verifying if search results page is displayed
	  strPageTitle = driver.getTitle();
	  Assert.assertEquals(strPageTitle, strSearchInput + " at DuckDuckGo");
	  
	  // Calling functions to verify search results
	  srp = new searchResPage(driver);
	  int resp = srp.verifySearchResultsPage(driver, strSearchInput);
	  if(resp > 1) {
		  System.out.println("Fizy website is not shown in top search result of " + resp + " pages in browser chrome for Search Input " + strSearchInput);
		  Assert.assertFalse(false);
	  }
  }
  
  @Parameters("browser")
  @BeforeClass
  public void beforeMethod(String browser) throws IOException {
	  id = new initializeDriver();
	  cfr = new ConfigFileReader();
	  driver = id.openURLInBrowser(browser);
	  driver.get(cfr.getApplicationUrl());
	  
  }

  @AfterClass
  public void afterMethod() {
	  if(!(driver == null))
		  driver.close();
  }

}
