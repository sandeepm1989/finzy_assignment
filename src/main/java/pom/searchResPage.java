package pom;

import utilities.*;

import java.util.List;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class searchResPage {
	WebDriver driver;
	public searchResPage(WebDriver driver) {
	    PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[@id='duckbar']")
	private WebElement divDuckBar;
	
	@FindBy(xpath = "//a[@data-testid='result-extras-url-link']")
	private List<WebElement> searchResults;
	
	@FindBy(xpath = "//a[text()='More results']")
	private WebElement lnkMoreResults;
	
	public int verifySearchResultsPage(WebDriver driver, String strSearchInput) throws InterruptedException {
		
		wrapperFunc wf = new wrapperFunc();
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		if(!wf.isElementDisplayed(divDuckBar))
			Assert.assertFalse(false, "Search Results Page is not displayed");
		
		String resp = "";
		int resCount = searchResults.size();
		int pageNum = 1;
		/*for(int i=0; i<resCount; i++) {
			wf.waitForElementToBeVisible(driver, lnkMoreResults);
			String strSiteName = wf.getTextForAWebElement(searchResults.get(i));
			if(strSiteName.contains("finzy")) {
				int pos = i+1;
				System.out.println("Finzy website appears at the Position : " + pos + " of Page : " + pageNum + " in browser : " + cap.getBrowserName() + " for Search Input " + strSearchInput);
				resp = "true " + pageNum; //Can use return here
				break;
			}
			if(i==resCount-1) {
				Thread.sleep(2000);
				wf.clickElement(lnkMoreResults, "More Results Button");
				Thread.sleep(2000);
				//wf.waitForListOfElementToBeVisible(driver, searchResults);
				//wf.waitForElementToBeVisible(driver, lnkMoreResults);
				pageNum++;
				resCount = searchResults.size();
			}
			if(pageNum>10) {
				System.out.println("Finzy website has not appeared in the first 10 pages");
				break;
			}
			
			
		}*/
		int index = 0;
		while(wf.waitForElementToBeVisible(driver, lnkMoreResults)) {
			
			for(int i=index; i<resCount; i++) {
				wf.waitForElementToBeVisible(driver, lnkMoreResults);
				String strSiteName = wf.getTextForAWebElement(searchResults.get(i));
				if(strSiteName.contains("finzy")) {
					int pos = i+1;
					System.out.println("Finzy website appears at the Position : " + pos + " of Page : " + pageNum + " in browser : " + cap.getBrowserName() + " for Search Input " + strSearchInput);
					return pageNum; //Can use return here
				}
				
			}
			
			index++;
			pageNum++;
			wf.clickElement(lnkMoreResults, "More Results Button");
			resCount = searchResults.size();
		}
		
		return pageNum;
		
	}

}