package pom;

import utilities.wrapperFunc;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class homePage {
	
	public homePage(WebDriver driver) {
	    PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//input[@type='text']")
	private WebElement txtSearchKey;
	
	@FindBy(xpath = "//*[@type='submit']")
	private WebElement btnGo;
	
	public void performDuckDuckGoSearch(String strSearchKey) {
		
		wrapperFunc wf = new wrapperFunc();
		
		wf.enterKeysInInputField(txtSearchKey, "Search textbox", strSearchKey);
		wf.clickElement(btnGo, "Go Button");
		
	}

}