package utilities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class wrapperFunc {
	
	
	public void clickElement(WebElement ele, String eleName) {
		
		if(isElementDisplayed(ele))
			if(isElementEnabled(ele))
				ele.click();
			else
				Assert.fail("Element '" + eleName + "' is disabled");
		else
			Assert.fail("Element '" + eleName + "' is not visible");
		
	}
	
	public boolean isElementDisplayed(WebElement ele) {
		
		boolean flag = false;
		if(ele.isDisplayed())
			flag = true;
		return flag;
		
	}
	
	public boolean isElementEnabled(WebElement ele) {
		
		boolean flag = false;
		if(ele.isEnabled())
			flag = true;
		return flag;
		
	}
	
	public void enterKeysInInputField(WebElement ele, String eleName, String strInputVal) {
		
		if(isElementDisplayed(ele))
			if(isElementEnabled(ele))
				ele.sendKeys(strInputVal);
			else
				Assert.fail("Element '" + eleName + "' is disabled");
		else
			Assert.fail("Element '" + eleName + "' is not visible");
		
	}
	
	public String getTextForAWebElement(WebElement ele) {
		
		return ele.getText();
		
	}
	
	public boolean waitForElementToBeVisible(WebDriver driver, WebElement ele) {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement w = wait.until(ExpectedConditions.visibilityOf(ele));
			if(!(w == null))
				flag = true;	
		}
		catch(Exception e) {
			flag = false;
		}
		return flag;
	}
	
	public void waitForListOfElementToBeVisible(WebDriver driver, List<WebElement> eles) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfAllElements(eles));
	}

}