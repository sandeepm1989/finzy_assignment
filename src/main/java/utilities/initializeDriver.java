package utilities;

import java.io.File;
import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class initializeDriver {
	
	WebDriver driver;
	dataReader dr;
	
	public WebDriver openURLInBrowser(String browser) throws IOException {
		
		dr = new dataReader();
		String chromeDriver = "", geckoDriver = "";
		String projPath = System.getProperty("user.dir");
		String OS = System.getProperty("os.name");
		if(OS.toLowerCase().contains("mac")) {
			chromeDriver = "/drivers/chromedriver";
			geckoDriver = "/drivers/geckodriver";
		} else if(OS.toLowerCase().contains("windows")) {
			chromeDriver = "/drivers/chromedriver.exe";
			geckoDriver = "/drivers/geckodriver.exe";
		}
		switch(browser.toLowerCase()) {
		case "chrome":	
			//Setting System Property of ChromeDriver with the Path where it is located
			System.setProperty("webdriver.chrome.driver",projPath + chromeDriver);
			// initialize new WebDriver session
		    driver = new ChromeDriver();
			break;
		case "firefox":	
			//Setting System Property of ChromeDriver with the Path where it is located
			System.setProperty("webdriver.gecko.driver",projPath + geckoDriver);
			// initialize new WebDriver session
		    driver = new FirefoxDriver();
			break;
		}
		driver.manage().window().maximize();
		return driver;
	}
	 
	
}
