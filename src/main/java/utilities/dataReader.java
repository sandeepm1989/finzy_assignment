package utilities;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class dataReader {
	
	public String readDataFromExcel(String sheetName, String strTestName, String strColName) throws IOException {
		
		String strTestData = "";
		//Path of the excel file
		FileInputStream fs = new FileInputStream("/Users/sandeepm/eclipse-workspace/assignment1/data/testData.xls");
		//Creating a workbook
		XSSFWorkbook workbook = new XSSFWorkbook(fs);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		int rowCount = sheet.getLastRowNum();
		for(int i=0; i<=rowCount; i++) {
			String strVal = sheet.getRow(i).getCell(0).toString();
			if(strVal.equalsIgnoreCase(strTestName)) {
				int colCount = sheet.getRow(i).getLastCellNum();
				for(int j=1; j<colCount; j++) {
					String strColVal = sheet.getRow(0).getCell(j).toString();
					if(strColVal.equalsIgnoreCase(strColName)) {
						strTestData = sheet.getRow(i).getCell(j).toString();
						return strTestData;
					}
				}
			}
		}
		
		return strTestData;
		
	}

}
