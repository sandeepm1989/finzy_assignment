package managers;

import org.openqa.selenium.WebDriver;

import pom.homePage;
import pom.searchResPage;


public class PageObjectManager {
	
	private WebDriver driver;
	private homePage hp;
	private searchResPage srp;

	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}

	public homePage getHomePage(){
		return (hp == null) ? hp = new homePage(driver) : hp;
	}
	
	public searchResPage getSearchResultsPage(){
		return (srp == null) ? srp = new searchResPage(driver) : srp;
	}
	

}
